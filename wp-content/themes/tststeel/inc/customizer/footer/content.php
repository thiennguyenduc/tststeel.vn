<?php
$section  = 'footer_content';
$priority = 1;
Kirki::add_field( 'tm-finance', array(
	'type'        => 'typography',
	'settings'    => 'footer_font',
	'description' => esc_html( __( 'Set up font settings for footer text', 'tm-finance' ) ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'font-style'     => array( 'bold', 'italic' ),
		'font-family'    => TM_FINANCE_SECONDARY_FONT,
		'font-size'      => '13px',
		'font-weight'    => '400',
		'line-height'    => '1.5',
		'letter-spacing' => '0em',
	),
	'choices'     => array(
		'font-style'     => true,
		'font-family'    => true,
		'font-size'      => true,
		'font-weight'    => true,
		'line-height'    => true,
		'letter-spacing' => true,
	),
	'output'      => array(
		array(
			'element' => '.site-footer, .site-footer p',
		),
	),
) );
Kirki::add_field( 'tm-finance', array(
	'type'        => 'color',
	'setting'     => 'footer_text_color',
	'label'       => esc_html( __( 'Text', 'tm-finance' ) ),
	'description' => esc_html( __( 'Choose color for footer text', 'tm-finance' ) ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '#858585',
	'transport'   => 'postMessage',
	'output'      => array(
		array(
			'element'  => '.site-footer',
			'property' => 'color',
		),
	),
	'js_vars'     => array(
		array(
			'element'  => '.site-footer',
			'function' => 'css',
			'property' => 'color',
		),
	),
) );
Kirki::add_field( 'tm-finance', array(
	'type'        => 'color',
	'setting'     => 'footer_link_color',
	'label'       => esc_html( __( 'Link', 'tm-finance' ) ),
	'description' => esc_html( __( 'Choose color for footer text link', 'tm-finance' ) ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '#CCCCCC',
	'transport'   => 'postMessage',
	'output'      => array(
		array(
			'element'  => '.site-footer a',
			'property' => 'color',
		),
	),
	'js_vars'     => array(
		array(
			'element'  => '.site-footer a',
			'function' => 'css',
			'property' => 'color',
		),
	),
) );
Kirki::add_field( 'tm-finance', array(
	'type'        => 'color',
	'setting'     => 'footer_link_color_hover',
	'label'       => esc_html( __( 'Link hover', 'tm-finance' ) ),
	'description' => esc_html( __( 'Choose color for footer text link hover', 'tm-finance' ) ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '#858585',
	'transport'   => 'postMessage',
	'output'      => array(
		array(
			'element'  => '.site-footer a:hover',
			'property' => 'color',
		),
	),
	'js_vars'     => array(
		array(
			'element'  => '.site-footer a:hover',
			'function' => 'css',
			'property' => 'color',
		),
	),
) );

Kirki::add_field( 'tm-finance', array(
	'type'        => 'typography',
	'settings'    => 'footer_widget_title',
	'description' => esc_html( __( 'Set up style settings for footer widget', 'tm-finance' ) ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'margin-bottom'  => '10px',
		'font-size'      => '16px',
		'padding-bottom' => '8px',
	),
	'choices'     => array(
		'margin-bottom'  => true,
		'font-size'      => true,
		'padding-bottom' => true,
	),
	'output'      => array(
		array(
			'element' => '.site-footer .widget-title',
		),
	),
) );

Kirki::add_field( 'tm-finance', array(
	'type'        => 'text',
	'setting'     => 'footer_widget_title_font_size',
	'label'       => esc_html( __( 'Font size', 'tm-finance' ) ),
	'description' => esc_html( __( 'Set up font size widget title', 'tm-finance' ) ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 16,
	'transport'   => 'postMessage',
	'output'      => array(
		array(
			'element'  => '.site-footer .widget-title',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
        'js_vars'     => array(
		array(
			'element'  => '.site-footer .widget-title',
			'function' => 'css',
			'property' => 'font-size',
		),
	),
) );

Kirki::add_field( 'tm-finance', array(
	'type'      => 'text',
	'setting'   => 'footer_widget_title_margin',
	'label'     => esc_html( __( 'Margin', 'tm-finance' ) ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => '0px 0px 10px 0px',
	'transport' => 'postMessage',
	'output'    => array(
		array(
			'element'  => '.site-footer .widget-title',
			'property' => 'margin',
		),
	),
	'js_vars'   => array(
		array(
			'element'  => '.site-footer .widget-title',
			'function' => 'css',
			'property' => 'margin',
		),
	),
) );

Kirki::add_field( 'tm-finance', array(
	'type'      => 'text',
	'setting'   => 'footer_widget_title_padding',
	'label'     => esc_html( __( 'Margin', 'tm-finance' ) ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => '0px 0px 8px 0px',
	'transport' => 'postMessage',
	'output'    => array(
		array(
			'element'  => '.site-footer .widget-title',
			'property' => 'padding',
		),
	),
	'js_vars'   => array(
		array(
			'element'  => '.site-footer .widget-title',
			'function' => 'css',
			'property' => 'padding',
		),
	),
) );